import sqlite3
import csv
import reader

if __name__ == "__main__":
    con = sqlite3.connect("data/pole_emploi.db")
    cursor = con.cursor()

    query = """
        select
            rncp.code,
            formation.label,
            certificateur.label,
            diplome.label,
            cpne.code_cpne,
            cpne.label,
            npec.npec_final,
            npec.status,
            npec.date
        from npec 
        join rncp on rncp.rncp_id = npec.rncp_id 
        join formation on formation.formation_id = rncp.formation_id 
        join cpne on cpne.code_cpne = npec.code_cpne 
        join diplome on diplome.diplome_id = rncp.diplome_id
        join certificateur on certificateur.certificateur_id = rncp.certificateur_id;
    """
    cursor.execute(query)
    with open("test/db.csv", "w") as db_out:
        csv_db_out = csv.writer(db_out)
        for i,row in enumerate(cursor.fetchall()):
            row = list(row)
            row[4] = float(row[4])
            row[6] = float(row[6])
            csv_db_out.writerow(row)
            

    
    with reader.XLReader("data/data.xlsx", 'Onglet 3 - référentiel NPEC', 3) as xlr:
        with open("test/xl.csv", "w") as xl_out:
            csv_xl_out = csv.writer(xl_out)
            for line in xlr.get_line():
                csv_xl_out.writerow((line.values()))

