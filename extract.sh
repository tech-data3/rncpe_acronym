#!/bin/bash

cd $(dirname $(realpath "$0"))
source venv/bin/activate
python rncpe_acronym/extract.py $@
deactivate