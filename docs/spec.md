# specs

## Exploration

### First through

Le fichier contient 4 onglet et nous pouvons y trouver ceci

```
['Me lire', 'Onglet 2 - global', 'Onglet 3 - référentiel NPEC', 'Onglet 4 - CPNE-IDCC']
Me lire ,max column =  11 ,max row =  39
Onglet 2 - global ,max column =  217 ,max row =  3818
Onglet 3 - référentiel NPEC ,max column =  9 ,max row =  812173
Onglet 4 - CPNE-IDCC ,max column =  3 ,max row =  437
```

Dans l'onglet 2 et 3 il y a les informations que l'on veut extraire,
il y a plus d'information dans l'onglet 3, on choisit donc celui ci (seul onglet contenant le status)

Ce que l'on peut remarquer c'est que les 4 premieres colonnes de chaque ligne sont repeter 213 fois
(nombre de code cnpe different). il y a cependant une exception a ceci, un nom de formation est different 
sur une ligne d'un paquet de 213.

dans la branche jevaistroploin il y a donc une erreur d'integrite des donnees etant donnee que le but etait 
d'optimiser le temps d'execution et pour ce faire, cette anomalie a ete ignore.

la branche main quand a elle optimise l'integrite des donnees en premier lieu, et la performance ensuite
le delta en temps d'execution est d'un ordre de grandeur negligeable (la branche a ete garder car celle ci contient une
amelioration du cache qui n'a pas encore ete transfere dans main).

### modeles

#### mcd
```plantuml
@startuml
skinparam componentStyle rectangle
[contrat d'apprentissage] --> [rncp]: concerne un diplome
[rncp] --> [certificateur]: sont certifiable par
[contrat d'apprentissage] --> [npec]: sont pris en charge a hauteur de
[npec] --> [date]: ont des date d'applicabilite
[npec] --> [iddc]: correspond a 

@end
```
#### mld
```plantuml
@startuml
json rncp {
   "rncp_id":"INT, PK",
   "code":"varchar",
   "libele_formation": "varchar",
   "diplome_id": "varchar",
   "certifieur_id": "INT, FK"
}

json certifieur {
   "certifieur_id":"INT, PK",
   "libele":"varchar"
}

json cpne {
   "code_cpne":"INT, PK",
   "libele":"varchar"
}

json npec {
    "rncp_id":"INT, PK FK",
    "cpne":"INT, PK, FK",
    "npec_final":"INT",
    "status":["A","CPNE", "R"],
    "date":"date"
}

json diplome {
     "diplome_id":"INT, PK",
     "libele":"VARCHAR"
}
rncp "1..*" - "1" certifieur
cpne "1" - "1..*" npec
rncp "1" - "1..*" npec
diplome "1..*" - "1" rncp
@enduml
```



