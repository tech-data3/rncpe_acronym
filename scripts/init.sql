DROP TABLE IF EXISTS cpne;
DROP TABLE IF EXISTS certificateur;
DROP TABLE IF EXISTS diplome;
DROP TABLE IF EXISTS formation;
DROP TABLE IF EXISTS rncp;
DROP TABLE IF EXISTS npec;

CREATE TABLE cpne(
    code_cpne INTEGER PRIMARY KEY NOT NULL,
    label VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE certificateur(
    certificateur_id INTEGER PRIMARY KEY,
    label VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE diplome(
    diplome_id INTEGER PRIMARY KEY,
    label VARCHAR(50) NOT NULL UNIQUE
);
CREATE TABLE formation(
    formation_id INTEGER PRIMARY KEY,
    label VARCHAR(100) UNIQUE
);

CREATE TABLE rncp(
    rncp_id INTEGER PRIMARY KEY,
    code VARCHAR(50) NOT NULL ,
    formation_id INTEGER NOT NULL REFERENCES formation(formation_id),
    diplome_id INTEGER REFERENCES diplome(diplome_id),
    certificateur_id INTEGER REFERENCES certificateur(certificateur_id),
    UNIQUE (code, formation_id)
);


CREATE TABLE npec(
    rncp_id INTEGER NOT NULL REFERENCES rncp(rncp_id),
    code_cpne INTEGER NOT NULL REFERENCES cpne(code_cpne),
    npec_final INTEGER NOT NULL,
    status CHAR(4) CHECK(status in ('A', 'CPNE', 'R')) NOT NULL DEFAULT 'A',
    `date` DATE NOT NULL,
    PRIMARY KEY (rncp_id, code_cpne)
);

