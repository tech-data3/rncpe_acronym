#!/bin/bash

cd $(dirname $(realpath "$0"))
echo $(pwd)
python3 -m venv ./venv

source venv/bin/activate
pip install -r requirements.txt
deactivate
