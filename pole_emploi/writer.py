import sqlite3


def hard_cache(function):
    cache = {}

    def wrapper(*args, id=None, **kwargs):

        data_hash = hash(tuple(v for k, v in sorted(kwargs.items())))

        if args[1] in cache:
            if data_hash in cache[args[1]]:
                return cache[args[1]][data_hash]
        else:
            cache[args[1]] = dict()

        result = function(*args, id=id, **kwargs)
        cache[args[1]].update({data_hash: result})
        return result

    return wrapper


class SQLiteWriter:
    connection = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.execute_insert_batch("npec", self.batch)
        self.cursor.close()
        SQLiteWriter.connection.commit()
        SQLiteWriter.connection.close()

    def __init__(self, path):
        if not SQLiteWriter.connection:
            SQLiteWriter.connection = sqlite3.connect(path)
        self.cursor = SQLiteWriter.connection.cursor()
        self.collection = {}
        self.batch = []

    @hard_cache
    def execute_insert(self, table, id=None, **data):
        fields = []
        for k in data.keys():
            # dict is unordered. is it possible that keys return keys in different way in each call
            fields.append(k)
        fields_str = ", ".join(f for f in fields)
        values_str =", ".join(f":{f}" for f in fields)
        query = f"INSERT INTO {table}({fields_str}) values ({values_str});"

        self.cursor.execute(
            query, data)
        if not id in data:
            return self.cursor.lastrowid
        return data[id]

    def execute_insert_batch(self, table, batch):
        fields = []
        for k in batch[0].keys():
            # dict is unordered. is it possible that keys return keys in different way in each call
            fields.append(k)
        fields_str = ", ".join(f for f in fields)
        values_str =", ".join(f":{f}" for f in fields)
        query = f"INSERT INTO {table}({fields_str}) values ({values_str});"
        self.cursor.executemany(
            query, batch)

    def write(self, data: dict):
        hash_line = hash(tuple(v for k, v in sorted(data.items()) if
                               k in ["certificateur", "code_rncp","libelle_du_diplome", "libelle_de_la_formation"]))
        if hash_line in self.collection:
            pks = self.collection.get(hash_line)
            pks["code_cpne"]= self.execute_insert("cpne", id="code_cpne", code_cpne=data.get("code_cpne"),
                                                      label=data.get("cpne"))
        else:
            pks = {"diplome_id": self.execute_insert("diplome", id="diplome_id",
                                                           label=data.get("libelle_du_diplome")),
                   "code_cpne": self.execute_insert("cpne", id="code_cpne", code_cpne=data.get("code_cpne"),
                                                          label=data.get("cpne")),
                   "certificateur_id": self.execute_insert("certificateur", id="certificateur_id",
                                                                 label=data.get("certificateur")),
                   "formation_id": self.execute_insert("formation", id="formation_id",
                                                             label=data.get("libelle_de_la_formation"))}

            pks["rncp_id"] = self.execute_insert("rncp", id="rncp_id", diplome_id=pks["diplome_id"],
                                                       certificateur_id=pks["certificateur_id"],
                                                       formation_id=pks["formation_id"], code=data.get("code_rncp"))
            self.collection = {hash_line: pks}

        self.batch.append(
        {
            "npec_final": data.get("npec_final"),
            "status": data.get("statut"),
            "date": data.get("date_dapplicabilite_des_npec"),
            "rncp_id": pks["rncp_id"],
            "code_cpne": pks["code_cpne"]
        })
