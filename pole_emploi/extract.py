import reader, writer


def main():
    with writer.SQLiteWriter("data/pole_emploi.db") as w:
        with reader.XLReader("data/data.xlsx", 'Onglet 3 - référentiel NPEC', 3) as xlr:
            for line in xlr.get_line():
                w.write(line)



if __name__ == '__main__':
    main()
